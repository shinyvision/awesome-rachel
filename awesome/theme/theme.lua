--[[

     Powerarrow Dark Awesome WM theme
     github.com/lcpz

--]]

local gears = require("gears")
local lain  = require("lain")
local awful = require("awful")
local wibox = require("wibox")
local dpi   = require("beautiful.xresources").apply_dpi
local volume_widget = require("awesome-wm-widgets.volume-widget.volume")
local mytable       = awful.util.table or gears.table -- 4.{0,1} compatibility
local xresources = require("beautiful.xresources")
local xtheme = xresources.get_current_theme()

local os = os
local my_table = awful.util.table or gears.table -- 4.{0,1} compatibility

local theme                                     = {}
theme.dir                                       = os.getenv("HOME") .. "/.config/awesome/theme"
theme.wallpaper                                 = theme.dir .. "/wall.jpg"
theme.font                                      = "Ubuntu 12"
theme.fg_normal                                 = xtheme.foreground
theme.fg_focus                                  = "#ffffffdd"
theme.fg_urgent                                 = "#CC9393"
theme.bg_normal                                 = xtheme.background
theme.bg_focus                                  = xtheme.color12
theme.bg_urgent                                 = "#1A1A1A"
theme.border_width                              = dpi(1)
theme.border_normal                             = xtheme.color0
theme.border_focus                              = xtheme.color4
theme.border_marked                             = xtheme.color4
theme.tasklist_bg_focus                         = xtheme.color12
theme.titlebar_bg_focus                         = xtheme.color12
theme.titlebar_bg_normal                        = theme.bg_normal
theme.titlebar_fg_focus                         = xtheme.color4
theme.menu_height                               = dpi(16)
theme.menu_width                                = dpi(140)
theme.menu_submenu_icon                         = theme.dir .. "/icons/submenu.png"
theme.taglist_bg_focus                          = xtheme.color4
theme.taglist_bg_occupied                       = theme.taglist_bg_focus .. "99"
theme.layout_tile                               = theme.dir .. "/icons/tile.png"
theme.layout_tileleft                           = theme.dir .. "/icons/tileleft.png"
theme.layout_tilebottom                         = theme.dir .. "/icons/tilebottom.png"
theme.layout_tiletop                            = theme.dir .. "/icons/tiletop.png"
theme.layout_fairv                              = theme.dir .. "/icons/fairv.png"
theme.layout_fairh                              = theme.dir .. "/icons/fairh.png"
theme.layout_spiral                             = theme.dir .. "/icons/spiral.png"
theme.layout_dwindle                            = theme.dir .. "/icons/dwindle.png"
theme.layout_max                                = theme.dir .. "/icons/max.png"
theme.layout_fullscreen                         = theme.dir .. "/icons/fullscreen.png"
theme.layout_magnifier                          = theme.dir .. "/icons/magnifier.png"
theme.layout_floating                           = theme.dir .. "/icons/floating.png"
theme.widget_ac                                 = theme.dir .. "/icons/ac.png"
theme.widget_battery                            = theme.dir .. "/icons/battery.png"
theme.widget_battery_low                        = theme.dir .. "/icons/battery_low.png"
theme.widget_battery_empty                      = theme.dir .. "/icons/battery_empty.png"
theme.widget_mem                                = theme.dir .. "/icons/mem.png"
theme.widget_cpu                                = theme.dir .. "/icons/cpu.png"
theme.widget_temp                               = theme.dir .. "/icons/temp.png"
theme.widget_net                                = theme.dir .. "/icons/net.png"
theme.widget_hdd                                = theme.dir .. "/icons/hdd.png"
theme.widget_music                              = theme.dir .. "/icons/note.png"
theme.widget_music_on                           = theme.dir .. "/icons/note_on.png"
theme.widget_vol                                = theme.dir .. "/icons/vol.png"
theme.widget_vol_low                            = theme.dir .. "/icons/vol_low.png"
theme.widget_vol_no                             = theme.dir .. "/icons/vol_no.png"
theme.widget_vol_mute                           = theme.dir .. "/icons/vol_mute.png"
theme.widget_vol_svg                            = theme.dir .. "/icons/volume-scalable.svg"
theme.widget_vol_mute_svg                       = theme.dir .. "/icons/volume-muted-scalable.svg"
theme.widget_mail                               = theme.dir .. "/icons/mail.png"
theme.widget_mail_on                            = theme.dir .. "/icons/mail_on.png"
theme.widget_power                              = theme.dir .. "/icons/powermenu-scalable.svg"
theme.tasklist_plain_task_name                  = true
theme.tasklist_disable_icon                     = true
theme.useless_gap                               = dpi(0)
theme.titlebar_close_button_focus               = theme.dir .. "/icons/titlebar/close_focus.png"
theme.titlebar_close_button_normal              = theme.dir .. "/icons/titlebar/close_normal.png"
theme.titlebar_ontop_button_focus_active        = theme.dir .. "/icons/titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active       = theme.dir .. "/icons/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive      = theme.dir .. "/icons/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive     = theme.dir .. "/icons/titlebar/ontop_normal_inactive.png"
theme.titlebar_sticky_button_focus_active       = theme.dir .. "/icons/titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active      = theme.dir .. "/icons/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive     = theme.dir .. "/icons/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive    = theme.dir .. "/icons/titlebar/sticky_normal_inactive.png"
theme.titlebar_floating_button_focus_active     = theme.dir .. "/icons/titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active    = theme.dir .. "/icons/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive   = theme.dir .. "/icons/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive  = theme.dir .. "/icons/titlebar/floating_normal_inactive.png"
theme.titlebar_maximized_button_focus_active    = theme.dir .. "/icons/titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active   = theme.dir .. "/icons/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = theme.dir .. "/icons/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = theme.dir .. "/icons/titlebar/maximized_normal_inactive.png"
theme.icon_theme                                = "/usr/share/icons/Tela-purple-dark"

local markup = lain.util.markup
local separators = lain.util.separators

local keyboardlayout = awful.widget.keyboardlayout:new()

-- Textclock
local clockicon = wibox.widget.imagebox(theme.widget_clock)
local clock = awful.widget.watch(
    "date +'%a %d %b %R'", 60,
    function(widget, stdout)
        widget:set_markup(" " .. markup.font(theme.font, stdout))
    end
)

-- Calendar
theme.cal = lain.widget.cal({
    attach_to = { clock },
    notification_preset = {
        font = "Ubuntu 10",
        fg   = theme.fg_normal,
        bg   = theme.bg_normal
    }
})

-- Mail IMAP check
local mailicon = wibox.widget.imagebox(theme.widget_mail)
--[[ commented because it needs to be set before use
mailicon:buttons(my_table.join(awful.button({ }, 1, function () awful.spawn(mail) end)))
theme.mail = lain.widget.imap({
    timeout  = 180,
    server   = "server",
    mail     = "mail",
    password = "keyring get mail",
    settings = function()
        if mailcount > 0 then
            widget:set_markup(markup.font(theme.font, " " .. mailcount .. " "))
            mailicon:set_image(theme.widget_mail_on)
        else
            widget:set_text("")
            mailicon:set_image(theme.widget_mail)
        end
    end
})
--]]

-- MPD
local musicplr = awful.util.terminal .. " -title Music -e ncmpcpp"
local mpdicon = wibox.widget.imagebox(theme.widget_music)
mpdicon:buttons(my_table.join(
    awful.button({ "Mod4" }, 1, function () awful.spawn(musicplr) end),
    awful.button({ }, 1, function ()
        os.execute("mpc prev")
        theme.mpd.update()
    end),
    awful.button({ }, 2, function ()
        os.execute("mpc toggle")
        theme.mpd.update()
    end),
    awful.button({ }, 3, function ()
        os.execute("mpc next")
        theme.mpd.update()
    end)))
theme.mpd = lain.widget.mpd({
    settings = function()
        if mpd_now.state == "play" then
            artist = " " .. mpd_now.artist .. " "
            title  = mpd_now.title  .. " "
            mpdicon:set_image(theme.widget_music_on)
        elseif mpd_now.state == "pause" then
            artist = " mpd "
            title  = "paused "
        else
            artist = ""
            title  = ""
            mpdicon:set_image(theme.widget_music)
        end

        widget:set_markup(markup.font(theme.font, markup("#EA6F81", artist) .. title))
    end
})

-- MEM
local memicon = wibox.widget.imagebox(theme.widget_mem)
local mem = lain.widget.mem({
    settings = function()
        widget:set_markup(markup.font(theme.font, " " .. mem_now.used .. "MB "))
    end
})

-- CPU
local cpuicon = wibox.widget.imagebox(theme.widget_cpu)
local cpu = lain.widget.cpu({
    settings = function()
        widget:set_markup(markup.font(theme.font, " " .. cpu_now.usage .. "% "))
    end
})

power_icon = wibox.widget.imagebox(theme.widget_power)
local powermenu = wibox.widget({
  {
    widget = power_icon,
    force_width = 24,
    force_height = 24,
    valign = 'center',
    halign = 'center',
  },
  widget = wibox.container.margin,
  margins = 4
})
power_icon.opacity = 0.7

powermenu:connect_signal('button::press', function()
    awful.spawn.with_shell("$HOME/.config/rofi/powermenu/powermenu.sh")
end
)

-- Separators
local spr     = wibox.widget.textbox(' ')
local arrl_dl = separators.arrow_left(theme.bg_focus, "alpha")
local arrl_ld = separators.arrow_left("alpha", theme.bg_focus)

function theme.at_screen_connect(s)
    awful.tag(awful.util.tagnames, s, awful.layout.layouts)

    s.mypromptbox = awful.widget.prompt()
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(my_table.join(
                           awful.button({}, 1, function () awful.layout.inc( 1) end),
                           awful.button({}, 2, function () awful.layout.set( awful.layout.layouts[1] ) end),
                           awful.button({}, 3, function () awful.layout.inc(-1) end),
                           awful.button({}, 4, function () awful.layout.inc( 1) end),
                           awful.button({}, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        widget_template = {
            {
                {
                    {
                        id     = 'index_role',
                        widget = wibox.widget.textbox,
                    },
                    layout = wibox.layout.fixed.horizontal,
                },
                left  = 10,
                right = 10,
                widget = wibox.container.margin
            },
            id     = 'background_role',
            widget = wibox.container.background,
            -- Add support for hover colors and an index label
            create_callback = function(self, c3, index, objects) --luacheck: no unused args
                self:get_children_by_id('index_role')[1].markup = '<b> '..index..' </b>'
                self:connect_signal('button::press', function()
                    self.backup = theme.taglist_bg_focus
                    self.has_backup = true
                end)
                self:connect_signal('mouse::enter', function()
                    if self.bg ~= theme.taglist_bg_focus then
                        self.backup     = self.bg
                        self.has_backup = true
                    end
                    self.bg = theme.taglist_bg_focus
                end)
                self:connect_signal('mouse::leave', function()
                    if self.has_backup then self.bg = self.backup end
                end)
            end,
            update_callback = function(self, c3, index, objects) --luacheck: no unused args
                self:get_children_by_id('index_role')[1].markup = '<b> '..index..' </b>'
            end,
        },
        buttons = awful.util.taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, function(c, screen)
        if c.class == 'Rofi' then
            return false
        end
        return awful.widget.tasklist.filter.currenttags(c, screen)
    end, awful.util.tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", ontop = true, screen = s, height = dpi(26), bg = theme.bg_normal, fg = theme.fg_normal })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            --spr,
            s.mytaglist,
            s.mypromptbox,
            spr,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            wibox.widget.systray(),
            keyboardlayout,
            spr,
            arrl_ld,
            wibox.container.background(mpdicon, theme.bg_focus),
            wibox.container.background(theme.mpd.widget, theme.bg_focus),
            arrl_dl,
            volume_widget(),
            arrl_ld,
            wibox.container.background(mailicon, theme.bg_focus),
            arrl_dl,
            memicon,
            mem.widget,
            arrl_ld,
            wibox.container.background(cpuicon, theme.bg_focus),
            wibox.container.background(cpu.widget, theme.bg_focus),
            arrl_dl,
            clock,
            spr,
            arrl_ld,
            wibox.container.background(s.mylayoutbox, theme.bg_focus),
            wibox.container.background(powermenu, theme.bg_focus),
        },
    }

    local volume_notifier = wibox {
        visible = false,
        width = 300,
        height = 125,
        bg = "#27262c",
        opacity = 0.9,
        shape = gears.shape.rounded_rect,
        type = "dialog",
        shape_args = {25, },
        ontop = true,
        x = s.geometry.width / 2 - 150,
        y = 100,
        layout = wibox.layout.align.horizontal
    }
    local function GET_VOLUME_CMD(device) return 'amixer -D ' .. device .. ' sget Master' end
    local volume_icon = wibox.widget {
            image = theme.widget_vol_svg,
            resize = false,
            forced_height = 64,
            forced_width = 64,
            widget = wibox.widget.imagebox
        }
    local volume_level = 0.1
    local volume_bar = wibox.widget {
      max_value     = 1,
      value         = volume_level,
      forced_height = 8,
      forced_width  = 250,
      background_color = theme.bg_normal,
      color = theme.bg_focus,
      shape         = gears.shape.rounded_bar,
      widget        = wibox.widget.progressbar,
    }
    local hide_timer = gears.timer { 
            timeout   = 2,
            call_now  = false,
            single_shot = true,
            autostart = false,
            callback  = function()
                volume_notifier.visible = false
            end
        }
    local update_bar_timer = gears.timer {
      timeout = 0.01,
      call_now = false,
      single_shot = true,
      autostart = false,
      callback = function()
        awful.spawn.easy_async(GET_VOLUME_CMD('pulse'), function(stdout)
          local mute = string.match(stdout, "%[(o%D%D?)%]")
          if mute == 'off' then
            volume_icon.image = theme.widget_vol_mute_svg
            volume_icon.opacity = 0.8
          elseif mute == 'on' then
            volume_icon.image = theme.widget_vol_svg
            volume_icon.opacity = 1
          end
          volume_level = string.match(stdout, "(%d?%d?%d)%%")
          volume_level = string.format("% 3d", volume_level)
          volume_bar.value = volume_level / 100
          if volume_notifier.visible == false then
            volume_notifier.visible = true
            volume_notifier.opacity = 0.9
          end
          hide_timer:stop()
          hide_timer:start()
        end)
      end
    }
    local function update_volume_bar()
      update_bar_timer:start()
    end
    awesome.connect_signal("volume_changed", function()
      update_volume_bar()
    end)
    volume_notifier:setup {
        {
          layout = wibox.layout.align.horizontal,
          expand = "outside",
          nil,
          {
            layout = wibox.layout.fixed.vertical,
            {
              widget = wibox.widget.separator,
              forced_height = 17,
              forced_width = 1,
              opacity = 0,
            },
            {
              layout = wibox.layout.align.horizontal,
              expand = "outside",
              nil,
              volume_icon,
            },
            {
              widget = wibox.widget.separator,
              forced_height = 10,
              forced_width = 1,
              opacity = 0,
            },
            volume_bar
          },
        },
        widget = wibox.container.margin,
    }
    volume_notifier:connect_signal("mouse::enter", function()
      volume_notifier.opacity = 0.4
    end)
    volume_notifier:connect_signal("mouse::leave", function()
      volume_notifier.opacity = 0.9
    end)

    client.connect_signal("manage", function (c)
        c.shape = function(cr,w,h)
            gears.shape.rounded_rect(cr,w,h,5)
        end
    end)
end

return theme
