#!/usr/bin/env bash
if pgrep -x rofi > /dev/null; then
  pkill rofi
else
  rofi -no-lazy-grab -show drun -modi drun -theme "$HOME/.config/rofi/launchers/misc/kde_simplemenu.rasi" -normal-window
fi
