#!/usr/bin/env bash

theme="full_circle"
dir="$HOME/.config/rofi/powermenu"

color="nordic.rasi"
theme="row_circle.rasi"

uptime=$(uptime -p | sed -e 's/up //g')

rofi_command="rofi -theme $dir/$theme"

# Options
shutdown=""
reboot=""
suspend=""
logout=""

# Confirmation
confirm_exit() {
	rofi -dmenu\
		-i\
		-no-fixed-num-lines\
		-p "Are You Sure? : "\
		-theme $dir/confirm.rasi
}

# Message
msg() {
	rofi -theme "$dir/message.rasi" -e "Available Options  -  yes / y / no / n"
}

# Variable passed to rofi
options="$shutdown\n$reboot\n$suspend\n$logout"

chosen="$(echo -e "$options" | $rofi_command -p "Uptime: $uptime" -dmenu -selected-row 0)"
case $chosen in
    $shutdown)

			systemctl poweroff

        ;;
    $reboot)

			systemctl reboot
      ;;
    $suspend)

			mpc -q pause
			amixer set Master mute
			systemctl suspend

        ;;
    $logout)
			pkill awesome
        ;;
esac
