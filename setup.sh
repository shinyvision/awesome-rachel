#!/bin/sh
if ! [ -x "$(command -v yay)" ]; then
	echo "Please install yay." && exit
fi	

sudo pacman --noconfirm -S rsync mpd xcape rofi scrot alsa-utils nitrogen mpc lxappearance awesome picom alacritty ttf-ubuntu-font-family
yay --noconfirm -S brave-bin nerd-fonts-fantasque-sans-mono
mkdir -p "$HOME/Pictures/screenshots"
rsync -avzh ./awesome $HOME/.config
rsync -avzh ./alacritty $HOME/.config
rsync -avzh ./rofi $HOME/.config
rsync -avzh ./picom $HOME/.config
cat ./.Xresources > $HOME/.Xresources

